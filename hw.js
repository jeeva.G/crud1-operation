import express from 'express'  
const app = express()
app.use(express.json())

const port = 3000
let students = [
  {
    name:'jeeva',
    rollnumber:'19111108',
    dob:'10-04-1999',
    dept:'mech'
  },
  {
    name: 'nantha',
    rollnumber: "23423435",
    dob: '11-04-2002',
    dept: 'CSE'
  }
]
app.get('/students', (req, res) => {
  res.status(200).send(students)
})
app.post('/students', (req, res) => {
  console.log(req.body);
  students.push(req.body);
  res.status(201).send(students)
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
app.put('/students', (req, res) => {
  console.log(req.body);
  students.push(req.body);
  res.status(200).send(students)
})
app.delete('/students/:name', (req, res) => {
 const student = students.filter((stu)=> stu.name !== req.params.name);

 console.log(students)
 res.status(200).send(student);
  
})



